Feature: User_Profile

  Background: User is already login
    Given user is in Login page
    When user input valid credentials in Login page
    And user click Login button
    Then user successfully login

  Scenario Outline: User edit profile on edit profile page
    When user tap button pencil
    And user edit <field> with <condition> and user tap button simpan
    Then user <result> edit profile

    Examples: 
      | case_id | field  | condition   | result                          |
      | MPRO1   | image  | valid value | Profile succesfully update      |
      | MPRO2   | nama   | empty value | The message appears wajib diisi |
      | MPRO3   | nama   | valid value | Profile succesfully update      |
      | MPRO4   | telpon | empty value | The message appears wajib diisi |
      | MPRO5   | telpon | valid value | Profile succesfully update      |
      | MPRO6   | kota   | empty value | The message appears wajib diisi |
      | MPRO7   | kota   | valid value | Profile succesfully update      |
      | MPRO8   | alamat | empty value | The message appears wajib diisi |
      | MPRO9   | alamat | valid value | Profile succesfully update      |
