Feature: Search Product

  Background: User view the Homepage and search for a product 
  	Given user is in the Homepage
  
  Scenario: MBUY1-User view product based on category
    When user click on one of the Category button
    Then page listed products based on selected category

  Scenario: MBUY2-User searches for a product
    When user click on Search field
    And user input product name
    And user select one of the product
    Then user will be redirected to Product Detail page

  Scenario: MBUY3-User bargain a product without login
    When user select one product
    And user click on Saya Tertarik dan Ingin Nego button
    Then user will be redirected to Login page