Feature: Buyer

  Background: User is already login
    Given user is in Login page
    When user input valid credentials in Login page
    And user click Login button
    Then user successfully login

  Scenario: MBUY4-User can successfully bargain a product
    When user view the Homepage
    And user select one product
    And user click on Saya Tertarik dan Ingin Nego button
    And user input Harga Tawar field
    And user click on Kirim button
    Then user will be successfully bargain

  Scenario: MBUY5-User check transaction status of the bargain
    When user click on menu Pesanan Saya
    Then page will be listed transaction status of the user