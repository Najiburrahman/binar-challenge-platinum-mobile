Feature: Register Mobile

  Scenario Outline: User Registration
    Given user is in Register page
    When user input <condition> in Register page
    And user click Register button
    Then user <result> register

    Examples: 
      | case_id | condition              | result                                         |
      | MREG001 | valid credentials      | successfully                                   |
      | MREG002 | empty email            | get empty email warning and can not            |
      | MREG003 | invalid email format   | get invalid email format warning and can not   |
      | MREG004 | existing email         | get existing email format warning and can not  |
      | MREG005 | empty credentials      | get empty name warning and can not             |