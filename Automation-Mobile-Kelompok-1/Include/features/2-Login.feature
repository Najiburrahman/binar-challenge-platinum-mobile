Feature: Login

  Scenario Outline: User fills email and password in login page
    Given user is in Login page
    When user input <condition> in Login page
    And user click Login button
    Then user <result> login

    Examples: 
      | case_id | condition                          | result                               |
      | MLOG1   | valid credentials                  | successfully                         |
      | MLOG2   | empty email                        | see email warning and failed         |
      | MLOG3   | incorrect email format             | see email format warning and failed  |
      | MLOG4   | empty password                     | see password warning and failed      |
      | MLOG5   | invalid pasword max char           | see password char warning and failed |
      | MLOG6   | valid email and invalid password   | see toast message and failed         |
      | MLOG7   | invalid email and valid password   | see toast message and failed         |
      | MLOG8   | invalid email and invalid password | see toast message and failed         |
