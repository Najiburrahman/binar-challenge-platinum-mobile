package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable
import cucumber.api.java.en.Given
import cucumber.api.java.en.When
import cucumber.api.java.en.Then

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import io.appium.java_client.AppiumDriver
import com.kms.katalon.core.util.KeywordUtil


public class Search_Product {

	@Given("user is in the Homepage")
	public void user_is_in_the_homepage() {
		Mobile.startApplication('Application/secondhand-24082023.apk', true)
	}

	@When("user click on one of the Category button")
	public void user_click_on_one_of_the_category_button() {
		Mobile.waitForElementPresent(findTestObject('Object Repository/Homepage/category-section'), 0)
		Mobile.tap(findTestObject('Object Repository/Homepage/select-category'), 0)
	}

	@Then("page listed products based on selected category")
	public void page_listed_products_based_on_selected_category() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/Homepage/result-search-category'), 0)
	}

	@When("user click on Search field")
	public void user_click_on_search_field() {
		Mobile.waitForElementPresent(findTestObject('Object Repository/Homepage/input-search'), 0)
		Mobile.tap(findTestObject('Object Repository/Homepage/input-search'), 0)
	}

	@When("user input product name")
	public void user_input_product_name() {
		Mobile.waitForElementPresent(findTestObject('Object Repository/Homepage/verify-search-image'), 0)
		Mobile.tap(findTestObject('Object Repository/Homepage/input-search-text'), 0)
		Mobile.setText(findTestObject('Object Repository/Homepage/input-search-text'), 'Ice Cream', 0)
		Mobile.waitForElementPresent(findTestObject('Object Repository/Homepage/result-search-product'), 0)
	}

	@When("user select one of the product")
	public void user_select_one_of_the_product() {
		Mobile.tap(findTestObject('Object Repository/Homepage/result-search-product'), 0)
	}

	@Then("user will be redirected to Product Detail page")
	public void user_will_be_redirected_to_product_detail_page() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/Product_Detail/btn-offer'), 0)
	}

	@When("user select one product")
	public void user_select_one_product() {
		Mobile.waitForElementPresent(findTestObject('Object Repository/Homepage/select-product'), 0)
		Mobile.tap(findTestObject('Object Repository/Homepage/select-product'), 0)
	}

	@When("user click on Saya Tertarik dan Ingin Nego button")
	public void user_click_on_saya_tertarik_dan_ingin_nego_button() {
		Mobile.waitForElementPresent(findTestObject('Object Repository/Product_Detail/btn-offer'), 0)
		Mobile.tap(findTestObject('Object Repository/Product_Detail/btn-offer'), 0)
	}

	@Then("user will be redirected to Login page")
	public void user_will_be_redirected_to_login_page() {
		AppiumDriver<?> driver = MobileDriverFactory.getDriver()

		driver.findElementByXPath("//android.widget.Toast[@text='Silakan login terlebih dahulu']")

		Mobile.verifyElementVisible(findTestObject('Object Repository/Login/input-email'), 0)
	}
}