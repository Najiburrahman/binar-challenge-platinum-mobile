package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import io.appium.java_client.AppiumDriver
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Register {

	//Verify if the user is already on Register page
	@Given("user is in Register page")
	public void user_is_in_register_page() {
		Mobile.startApplication("Application/secondhand-24082023.apk", true)
		Mobile.tap(findTestObject('Object Repository/Homepage/btn-akun'), 0)
		Mobile.tap(findTestObject('Object Repository/Akun_Saya/btn-masuk-akun'), 0)
		Mobile.tap(findTestObject('Object Repository/Login/text-daftar'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/Register/btn-daftar'), 0)

	}

	//Successful and failed Register actions
	@When("user input (.*) in Register page")
	public void user_input_in_register_page(String condition) {
		if(condition=="valid credentials") {
			Mobile.setText(findTestObject('Object Repository/Register/form-nama'), 'Richardo', 0)
			int RandomNumber;
			RandomNumber = (int)(Math.random()*1000)
			Mobile.setText(findTestObject('Object Repository/Register/form-email'), 'Richard'+RandomNumber+'@gmail.com', 0)
			Mobile.setText(findTestObject('Object Repository/Register/form-password'), '12345678', 0)
			Mobile.setText(findTestObject('Object Repository/Register/form-phone-number'), '6287123456789', 0)
			Mobile.setText(findTestObject('Object Repository/Register/form-city'), 'Jakarta', 0)
			Mobile.setText(findTestObject('Object Repository/Register/form-address'), 'Jl. Jalan no.1', 0)
		}else if(condition=="empty email") {
			Mobile.setText(findTestObject('Object Repository/Register/form-nama'), 'Richardo', 0)
			Mobile.setText(findTestObject('Object Repository/Register/form-password'), '12345678', 0)
			Mobile.setText(findTestObject('Object Repository/Register/form-phone-number'), '6287123456789', 0)
			Mobile.setText(findTestObject('Object Repository/Register/form-city'), 'Jakarta', 0)
			Mobile.setText(findTestObject('Object Repository/Register/form-address'), 'Jl. Jalan no.1', 0)
		}else if(condition=="invalid email format") {
			Mobile.setText(findTestObject('Object Repository/Register/form-nama'), 'Richardo', 0)
			Mobile.setText(findTestObject('Object Repository/Register/form-email'), 'Richardgmailcom', 0)
			Mobile.setText(findTestObject('Object Repository/Register/form-password'), '12345678', 0)
			Mobile.setText(findTestObject('Object Repository/Register/form-phone-number'), '6287123456789', 0)
			Mobile.setText(findTestObject('Object Repository/Register/form-city'), 'Jakarta', 0)
			Mobile.setText(findTestObject('Object Repository/Register/form-address'), 'Jl. Jalan no.1', 0)
		}else if(condition=="existing email") {
			Mobile.setText(findTestObject('Object Repository/Register/form-nama'), 'Richardo', 0)
			Mobile.setText(findTestObject('Object Repository/Register/form-email'), 'richardo.gabriel@gmail.com', 0)
			Mobile.setText(findTestObject('Object Repository/Register/form-password'), '12345678', 0)
			Mobile.setText(findTestObject('Object Repository/Register/form-phone-number'), '6287123456789', 0)
			Mobile.setText(findTestObject('Object Repository/Register/form-city'), 'Jakarta', 0)
			Mobile.setText(findTestObject('Object Repository/Register/form-address'), 'Jl. Jalan no.1', 0)
		}else if(condition=="empty credentials") {
		}
	}

	//Click register button to finish registration
	@When ("user click Register button")
	public void user_click_Register_button() {
		Mobile.tap(findTestObject('Object Repository/Register/btn-daftar'), 0)
	}

	//Expected results for the scenarios above
	@Then("user (.*) register")
	public void user_register(String result) {
		if(result=="successfully") {
			Mobile.verifyElementVisible(findTestObject('Object Repository/Akun_Saya/akun-nama'), 0)
		}else if(result=="get empty email warning and can not") {
			Mobile.getAttribute(findTestObject('Object Repository/Register/empty-email-msg'), 'text', 0)
		}else if(result=="get invalid email format warning and can not") {
			Mobile.getAttribute(findTestObject('Object Repository/Register/invalid-email-msg'), 'text', 0)
		}else if(result=="get existing email format warning and can not") {
			AppiumDriver<?> driver = MobileDriverFactory.getDriver()

			driver.findElementByXPath("//android.widget.Toast[@text='Email sudah digunakan']")
		}else if(result=="get empty name warning and can not") {
			Mobile.getAttribute(findTestObject('Object Repository/Register/empty-name-msg'), 'text', 0)
		}
	}
}