package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable
import cucumber.api.java.en.Given
import cucumber.api.java.en.When
import cucumber.api.java.en.Then


public class Buyer {

	@When("user view the Homepage")
	public void user_view_the_homepage() {
		Mobile.tap(findTestObject('Object Repository/Homepage/btn-beranda'), 0)
	}

	@When("user input Harga Tawar field")
	public void user_input_harga_tawar_field() {
		Mobile.waitForElementPresent(findTestObject('Object Repository/Product_Detail/input-offer-price'), 0)
		Mobile.tap(findTestObject('Object Repository/Product_Detail/input-offer-price'), 0)
		Mobile.setText(findTestObject('Object Repository/Product_Detail/input-offer-price'), '8000', 0)
		Mobile.hideKeyboard()
	}

	@When("user click on Kirim button")
	public void user_click_on_kirim_button() {
		Mobile.waitForElementPresent(findTestObject('Object Repository/Product_Detail/btn-send'), 0)
		Mobile.tap(findTestObject('Object Repository/Product_Detail/btn-send'), 5)
	}

	@Then("user will be successfully bargain")
	public void user_will_be_successfully_bargain() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/Product_Detail/verify-offer'), 5)
	}

	@When("user click on menu Pesanan Saya")
	public void user_click_on_menu_pesanan_saya() {
		Mobile.tap(findTestObject('Object Repository/Homepage/btn-pesanan-saya'), 0)
	}

	@Then("page will be listed transaction status of the user")
	public void page_will_be_listed_transaction_status_of_the_user() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/Homepage/page-pesanan-saya'), 0)
	}
}