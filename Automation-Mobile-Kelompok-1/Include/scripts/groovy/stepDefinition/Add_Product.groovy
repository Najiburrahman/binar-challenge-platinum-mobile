package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import io.appium.java_client.AppiumDriver
import com.kms.katalon.core.util.KeywordUtil

public class Add_Product {
	@When("user is in Homepage")
	public void user_is_in_Homepage() {
		Mobile.tap(findTestObject('Object Repository/Homepage/btn-beranda'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/Homepage/btn-jual'), 0)
	}
	@When("user tap on the Jual button")
	public void user_tap_on_the_Jual_button() {
		Mobile.tap(findTestObject('Object Repository/Homepage/btn-jual'), 0)
	}
	@When("user view the display of Add Product page")
	public void user_view_the_display_of_Add_Product_page() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/Add_Product/btn-terbitkan'), 0)
	}
	@When("user fill all the required fields with (.*)")
	public void user_fill_all_the_required_fields_with(String condition) {
		if(condition=="valid format") {
			Mobile.setText(findTestObject('Object Repository/Add_Product/input-product-name'), 'Pertama', 0)
			Mobile.setText(findTestObject('Object Repository/Add_Product/input-product-price'), '123', 0)
			Mobile.tap(findTestObject('Object Repository/Add_Product/input-category-dropdown'), 0)
			Mobile.delay(5)
			Mobile.tap(findTestObject('Object Repository/Add_Product/input-product-location'), 0)
			Mobile.delay(5)
			Mobile.setText(findTestObject('Object Repository/Add_Product/input-product-location'), 'Sleman', 0)
			Mobile.setText(findTestObject('Object Repository/Add_Product/input-description'), 'Barang Joss', 0)
			Mobile.waitForElementPresent(findTestObject('Object Repository/Add_Product/input-description'), 0)
			Mobile.tap(findTestObject('Object Repository/Add_Product/input-photos'), 0)
			Mobile.tap(findTestObject('Object Repository/Add_Product/btn-galeri'), 0)
			Mobile.waitForElementPresent(findTestObject('Object Repository/Add_Product/img-patrick'), 0)
			Mobile.tap(findTestObject('Object Repository/Add_Product/img-patrick'), 0)
		}else if(condition=="photo file size more than 10Mb") {
			Mobile.setText(findTestObject('Object Repository/Add_Product/input-product-name'), 'Kedua', 0)
			Mobile.setText(findTestObject('Object Repository/Add_Product/input-product-price'), '123', 0)
			Mobile.tap(findTestObject('Object Repository/Add_Product/input-category-dropdown'), 0)
			Mobile.delay(5)
			Mobile.tap(findTestObject('Object Repository/Add_Product/input-product-location'), 0)
			Mobile.delay(5)
			Mobile.setText(findTestObject('Object Repository/Add_Product/input-product-location'), 'Sleman', 0)
			Mobile.setText(findTestObject('Object Repository/Add_Product/input-description'), 'Barang Joss', 0)
			Mobile.waitForElementPresent(findTestObject('Object Repository/Add_Product/input-description'), 0)
			Mobile.tap(findTestObject('Object Repository/Add_Product/input-photos'), 0)
			Mobile.tap(findTestObject('Object Repository/Add_Product/btn-galeri'), 0)
			Mobile.waitForElementPresent(findTestObject('Object Repository/Add_Product/img-venom'), 0)
			Mobile.tap(findTestObject('Object Repository/Add_Product/img-venom'), 0)
		}else if(condition=="except Product Name field blank") {
			Mobile.setText(findTestObject('Object Repository/Add_Product/input-product-name'), '', 0)
			Mobile.setText(findTestObject('Object Repository/Add_Product/input-product-price'), '123', 0)
			Mobile.tap(findTestObject('Object Repository/Add_Product/input-category-dropdown'), 0)
			Mobile.delay(5)
			Mobile.tap(findTestObject('Object Repository/Add_Product/input-product-location'), 0)
			Mobile.delay(5)
			Mobile.setText(findTestObject('Object Repository/Add_Product/input-product-location'), 'Sleman', 0)
			Mobile.setText(findTestObject('Object Repository/Add_Product/input-description'), 'Barang Joss', 0)
			Mobile.waitForElementPresent(findTestObject('Object Repository/Add_Product/input-description'), 0)
			Mobile.tap(findTestObject('Object Repository/Add_Product/input-photos'), 0)
			Mobile.tap(findTestObject('Object Repository/Add_Product/btn-galeri'), 0)
			Mobile.waitForElementPresent(findTestObject('Object Repository/Add_Product/img-patrick'), 0)
			Mobile.tap(findTestObject('Object Repository/Add_Product/img-patrick'), 0)
		}else if(condition=="except Category field blank") {
			Mobile.setText(findTestObject('Object Repository/Add_Product/input-product-name'), 'Keempat', 0)
			Mobile.setText(findTestObject('Object Repository/Add_Product/input-product-price'), '123', 0)
			Mobile.setText(findTestObject('Object Repository/Add_Product/input-product-location'), 'Sleman', 0)
			Mobile.setText(findTestObject('Object Repository/Add_Product/input-description'), 'Barang Joss', 0)
			Mobile.waitForElementPresent(findTestObject('Object Repository/Add_Product/input-description'), 0)
			Mobile.tap(findTestObject('Object Repository/Add_Product/input-photos'), 0)
			Mobile.tap(findTestObject('Object Repository/Add_Product/btn-galeri'), 0)
			Mobile.waitForElementPresent(findTestObject('Object Repository/Add_Product/img-patrick'), 0)
			Mobile.tap(findTestObject('Object Repository/Add_Product/img-patrick'), 0)
		}else if(condition=="except Description field blank") {
			Mobile.setText(findTestObject('Object Repository/Add_Product/input-product-name'), 'Kelima', 0)
			Mobile.setText(findTestObject('Object Repository/Add_Product/input-product-price'), '123', 0)
			Mobile.tap(findTestObject('Object Repository/Add_Product/input-category-dropdown'), 0)
			Mobile.delay(5)
			Mobile.tap(findTestObject('Object Repository/Add_Product/input-product-location'), 0)
			Mobile.delay(5)
			Mobile.setText(findTestObject('Object Repository/Add_Product/input-product-location'), 'Sleman', 0)
			Mobile.setText(findTestObject('Object Repository/Add_Product/input-description'), '', 0)
			Mobile.waitForElementPresent(findTestObject('Object Repository/Add_Product/input-description'), 0)
			Mobile.tap(findTestObject('Object Repository/Add_Product/input-photos'), 0)
			Mobile.tap(findTestObject('Object Repository/Add_Product/btn-galeri'), 0)
			Mobile.waitForElementPresent(findTestObject('Object Repository/Add_Product/img-patrick'), 0)
			Mobile.tap(findTestObject('Object Repository/Add_Product/img-patrick'), 0)
		}
	}
	@When("user click the Terbitkan button")
	public void user_click_the_Terbitkan_button() {
		Mobile.tap(findTestObject('Object Repository/Add_Product/btn-terbitkan'), 0)
	}
	@Then("(.*) displayed")
	public void displayed(String result) {
		if(result=="user successfully add product and redirected to Product page") {
			Mobile.verifyElementVisible(findTestObject('Object Repository/Add_Product/btn-delete-after-add-product'), 0)
		}else if(result=="request entity too large") {
			AppiumDriver<?> driver = MobileDriverFactory.getDriver()
			driver.findElementsByXPath("//android.widget.Toast[@text='Request Entity Too Large']")
		}else if(result=="product Name field cant be blank") {
			Mobile.verifyElementVisible(findTestObject('Object Repository/Add_Product/input-product-name-cant-blank'), 0)
		}else if(result=="category field cant be blank") {
			AppiumDriver<?> driver = MobileDriverFactory.getDriver()
			driver.findElementsByXPath("//android.widget.Toast[@text='Pilih minimal 1 kategori']")
		}else if(result=="description field cant be blank") {
			Mobile.waitForElementPresent(findTestObject('Object Repository/Add_Product/input-product-name-cant-blank'), 0)
			Mobile.verifyElementVisible(findTestObject('Object Repository/Add_Product/input-product-name-cant-blank'), 0)
		}
		Mobile.closeApplication()
	}
}
