package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



public class User_Profile {

	@When("user tap button pencil")
	public void user_tap_button_pencil() {
		Mobile.tap(findTestObject('Object Repository/User_Profile/icon-pensil'), 0)
	}

	@When("user edit (.*) with (.*) and user tap button simpan")
	public void user_edit_with_and_user_tap_button_simpan(String field, condition) {
		if(field=="image" && condition=="valid value") {
			Mobile.tap(findTestObject('Object Repository/User_Profile/tap-image'), 0)
			Mobile.tap(findTestObject('Object Repository/User_Profile/btn-galeri'), 0)
			Mobile.waitForElementPresent(findTestObject('Object Repository/User_Profile/gambar'), 0)
			Mobile.tap(findTestObject('Object Repository/User_Profile/gambar'), 0)
		}
		else if(field=="nama" && condition=="empty value") {
			Mobile.tap(findTestObject('Object Repository/User_Profile/tap-nama'),0)
			Mobile.tap(findTestObject('Object Repository/User_Profile/tap-edit-nama'),0)
			Mobile.setText(findTestObject('Object Repository/User_Profile/tap-edit-nama'), '', 0)
			Mobile.getAttribute(findTestObject('Object Repository/User_Profile/tap-edit-nama'), 'text', 0)
		}
		else if (field=="nama" && condition=="valid value") {
			Mobile.tap(findTestObject('Object Repository/User_Profile/tap-nama'),0)
			Mobile.tap(findTestObject('Object Repository/User_Profile/tap-edit-nama'),0)
			Mobile.clearText(findTestObject('Object Repository/User_Profile/tap-edit-nama'), 0)
			Mobile.setText(findTestObject('Object Repository/User_Profile/tap-edit-nama'), 'Binar Akademi', 0)
			Mobile.pressBack()
			Mobile.tap(findTestObject('Object Repository/User_Profile/btn-simpan'), 0)
		}
		else if (field=="telpon" && condition=="empty value") {
			Mobile.tap(findTestObject('Object Repository/User_Profile/tap-no-hp'),0)
			Mobile.tap(findTestObject('Object Repository/User_Profile/tap-edit-no-hp'),0)
			Mobile.setText(findTestObject('Object Repository/User_Profile/tap-edit-no-hp'), '', 0)
			Mobile.getAttribute(findTestObject('Object Repository/User_Profile/tap-edit-no-hp'), 'text', 0)
		}
		else if (field=="telpon" && condition=="valid value") {
			Mobile.tap(findTestObject('Object Repository/User_Profile/tap-no-hp'),0)
			Mobile.clearText(findTestObject('Object Repository/User_Profile/tap-edit-no-hp'), 0)
			Mobile.setText(findTestObject('Object Repository/User_Profile/tap-edit-no-hp'), '0812345678', 0)
			Mobile.tap(findTestObject('Object Repository/User_Profile/btn-simpan'), 0)
		}
		else if (field=="kota" && condition=="empty value") {
			Mobile.tap(findTestObject('Object Repository/User_Profile/tap-kota'),0)
			Mobile.tap(findTestObject('Object Repository/User_Profile/tap-edit-kota'),0)
			Mobile.setText(findTestObject('Object Repository/User_Profile/tap-edit-kota'), '', 0)
			Mobile.getAttribute(findTestObject('Object Repository/User_Profile/tap-edit-kota'), 'text', 0)
		}
		else if (field=="kota" && condition=="valid value") {
			Mobile.tap(findTestObject('Object Repository/User_Profile/tap-kota'),0)
			Mobile.clearText(findTestObject('Object Repository/User_Profile/tap-edit-kota'), 0)
			Mobile.setText(findTestObject('Object Repository/User_Profile/tap-edit-kota'), 'kota jakarta', 0)
			Mobile.tap(findTestObject('Object Repository/User_Profile/btn-simpan'), 0)
		}
		else if (field=="alamat" && condition=="empty value") {
			Mobile.tap(findTestObject('Object Repository/User_Profile/tap-alamat'),0)
			Mobile.tap(findTestObject('Object Repository/User_Profile/tap-edit-alamat'),0)
			Mobile.setText(findTestObject('Object Repository/User_Profile/tap-edit-alamat'), '', 0)
			Mobile.getAttribute(findTestObject('Object Repository/User_Profile/tap-edit-alamat'), 'text', 0)
		}
		else if (field=="alamat" && condition=="valid value") {
			Mobile.tap(findTestObject('Object Repository/User_Profile/tap-alamat'),0)
			Mobile.clearText(findTestObject('Object Repository/User_Profile/tap-edit-alamat'), 0)
			Mobile.setText(findTestObject('Object Repository/User_Profile/tap-edit-alamat'), 'jakarta utara', 0)
			Mobile.tap(findTestObject('Object Repository/User_Profile/btn-simpan'), 0)
		}
	}
	@Then("user (.*) edit profile")
	public void user_edit_profile(String result) {
		if(result=="Profile succesfully update") {
			Mobile.verifyElementVisible(findTestObject('Object Repository/User_Profile/tap-nama'), 0)
			Mobile.closeApplication()
		}
		else if(result=="can not update profile") {
			Mobile.getAttribute(findTestObject('Object Repository/User_Profile/tap-edit-nama'), 'wajib diisi', 0)
			Mobile.getAttribute(findTestObject('Object Repository/User_Profile/tap-no-hp'), 'wajib diisi', 0)
			Mobile.getAttribute(findTestObject('Object Repository/User_Profile/tap-kota'), 'wajib diisi', 0)
			Mobile.getAttribute(findTestObject('Object Repository/User_Profile/tap-alamat'), 'wajib diisi', 0)
			Mobile.verifyElementVisible(findTestObject('Object Repository/User_Profile/tap-password'), 0)
			Mobile.verifyElementVisible(findTestObject('Object Repository/User_Profile/tap-email'), 0)
			Mobile.closeApplication()
		}
	}
}


